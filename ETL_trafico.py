import json
import urllib.request as req
import xmltodict
import urllib3
from common import dateutils as dtu, key_manager as km
from devo.sender import SenderConfigSSL, Sender
import datetime
from devo.api import Client, JSON
import pickle
import os


def get_rows(file):
    rows = []
    with open(file) as file:
        data = xmltodict.parse(file.read())
        date_time = dtu.get_utc_datetime(data['pms']['fecha_hora'], date_separator='/', reversed_date=True,
                                         offset=False)
        for pm in data['pms']['pm']:
            if pm['error'] == 'N' and int(pm['intensidad']) > 0 and int(pm['ocupacion']) > 0 and int(pm['carga']) > 0:
                id = pm['idelem']
                intensidad = pm['intensidad']
                ocupacion = pm['ocupacion']
                carga = pm['carga']
                msg = str(date_time) + '|' + id + ';' + intensidad + ';' + ocupacion + ';' + carga
                if 'intensidadSat' in pm:
                    msg += ';' + pm['intensidadSat']
                else:
                    msg += ';'
                if 'velocidad' in pm:
                    msg += ';' + pm['velocidad']
                else:
                    msg += ';'
                if 'nivelServicio' in pm:
                    msg += ';' + get_nivelServicio(pm['nivelServicio'])
                else:
                    msg += ';'
                if intensidad != '-1' or ocupacion != '-1' or carga != '-1':
                    rows.append(msg)
    return rows


def get_nivelServicio(nivel):
    nivelServicio = {'0': 'trafico fluido',
                     '1': 'trafico lento',
                     '2': 'retenciones',
                     '3': 'congestion'}
    if nivel in nivelServicio:
        return nivelServicio[nivel]
    else:
        return ''


def get_file():
    mngr = urllib3.PoolManager()
    response = mngr.request('GET', 'https://datos.madrid.es/egob/catalogo/keyword/trafico.json')

    data = json.loads(response.data.decode('utf-8'))

    for k in data['result']['items']:
        if k['_about'] == 'https://datos.madrid.es/egob/catalogo/202087-0-trafico-intensidad':
            dist = k['distribution']

    for i in dist:
        if 'xml' in i['format']['value']:
            file_url = i['accessURL']
    try:
        req.urlretrieve(file_url, 'data sources/trafico.xml')
    except:
        print('Unable to get file')


def new_data(row):
    result = True
    last_sent = ''
    try:
        with open('/home/angelfombellida/TFG/pruebas/files/state-trafico', 'r') as f:
            last_sent = f.read()
    except FileNotFoundError:
        print('File: state-trafico.pkl will be created in /home/angelfombellida/TFG/pruebas/files')
    finally:
        with open('/home/angelfombellida/TFG/pruebas/files/state-trafico', 'w') as f:
            if last_sent == row.split('|')[0]:
                f.write(row.split('|')[0])
                result = False
            else:
                f.write(row.split('|')[0])

    return result


def set_correlation(rows):
    with open('data sources/lookup_correlation/estaciones-trafico.pkl', 'rb') as f:
        stations_correlation = pickle.load(f)
    result = []
    for row in rows:
        fields = row.split(';')
        if fields[1] in stations_correlation:
            fields[1] = stations_correlation[fields[1]]
        result.append(';'.join(fields))
    return result


engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)

get_file()

rows = get_rows('data sources/trafico.xml')
sent = 0
if new_data(rows[0]):
    rows = km.add_key(rows, '/home/angelfombellida/TFG/pruebas/files/key-trafico.txt')
    rows = set_correlation(rows)
    for row in rows:
        eventdate = row.split('|')[0]
        msg = row.split('|')[1]
        con.send('(usd)my.app.aytomadrid.trafico', msg, date=eventdate)
        sent += 1

if sent > 0:
    print("\n[" + str(datetime.datetime.now()) + "] - Trafico: EXECUTION FINISHED: " + str(sent) + " lines were sent")
else:
    print("\n[" + str(datetime.datetime.now()) + "] - Trafico: nothing new")
