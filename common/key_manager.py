from datetime import datetime as dt


def autoincrement(last_key):
    current_date = dt.today().strftime('%y%m%d')

    if last_key == '':
        return current_date + '-1'
    date = last_key.split('-')[0]
    number = int(last_key.split('-')[1])
    if current_date == date:
        return current_date + '-' + str(number + 1)
    else:
        return current_date + '-1'


def add_key(rows, file):
    result_rows = []
    last_key = ''
    try:
        with open(file, 'r') as f:
            last_key = f.read()
    except FileNotFoundError:
        print('File: '+file.split('/')[-1]+' will be created in'+'/'.join(file.split('/')[:-1]))
    finally:
        for row in rows:
            last_key = autoincrement(last_key)
            result_rows.append(row.split('|')[0] + '|' + last_key + ';' + row.split('|')[1])
        with open(file, 'w') as f:
            f.write(last_key)
        return result_rows


def add_lookup_key(rows, file):
    result_rows=[]
    key=0
    for row in rows:
        key+=1
        result_rows.append(str(key)+';'+row)
    return result_rows