import datetime
import pytz


def get_datetime(dt, date_separator='-', middle_separator=' ', reversed_date=False):
    if reversed_date:
        date_format = '%d' + date_separator + '%m' + date_separator + '%Y' + middle_separator + '%H:%M:%S'
    else:
        date_format = '%Y' + date_separator + '%m' + date_separator + '%d' + middle_separator + '%H:%M:%S'
    return datetime.datetime.strptime(dt, date_format)


def to_utc(date):
    local = pytz.timezone("Europe/Madrid")
    local_dt = local.localize(date, is_dst=None)
    utc_dt = local_dt.astimezone(pytz.utc)
    return utc_dt


def get_utc_datetime(dt, date_separator='-', middle_separator=' ', reversed_date=False, offset=True):
    if offset:
        return to_utc(get_datetime(dt, date_separator, middle_separator, reversed_date))
    else:
        utc = to_utc(get_datetime(dt, date_separator, middle_separator, reversed_date))
        str_date = str(utc).split('+')[0]
        return get_datetime(str_date)