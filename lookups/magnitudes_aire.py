from devo.sender import SenderConfigSSL, Sender, Lookup
import json
import pickle

def save_correlation(file_path):
    correlation = {}
    with open(file_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            correlation[line.split(';')[1]] = line.split(';')[0]
    with open('/data sources/lookup_correlation/magnitudes-aire.pkl','wb') as f:
        pickle.dump(correlation,f)

engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)
save_correlation('/data sources/lookups/magnitudes_aire.csv')
lookup = Lookup('magnitudes_aire', historic_tag=None, con=con)

lookup.send_csv('/data sources/lookups/magnitudes_aire.csv', has_header=False, delimiter=';',
                headers=['MagnitudKey', 'numero', 'nombre', 'abreviatura'], key='MagnitudKey')