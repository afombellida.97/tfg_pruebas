from devo.sender import SenderConfigSSL, Sender, Lookup
import urllib3
import urllib.request as req
import json
import xlrd
import os
from common import key_manager as km
import pickle

engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)


def get_file():
    r = urllib3.PoolManager().request('GET', 'https://datos.madrid.es/egob/catalogo/keyword/estaciones.json')
    data = json.loads(r.data.decode('utf-8'))

    for item in data['result']['items']:
        if item['_about'] == 'https://datos.madrid.es/egob/catalogo/212629-0-estaciones-control-aire':
            dist = item['distribution']
    file_url = dist[0]['accessURL']
    req.urlretrieve(file_url, '/data sources/lookups/estaciones_aire.xls')


def get_type(type):
    types = {'UT': 'Urbana de trafico',
             'UF': 'Urbana de Fondo',
             'S': 'Suburbana'}
    if type in types:
        return types[type]
    else:
        return ''


def dms_to_decimal(coord):
    numbers = coord.split(' ')
    deg = numbers[0][:-1]
    min = numbers[1][:-1]
    sec = numbers[2][:-3].replace(',', '.')
    orientation = numbers[2][-1]
    decimal_value = int(deg) + float(min) / 60 + float(sec) / 3600
    if (orientation is 'O' or orientation is 'W' or orientation is 'S'):
        return -decimal_value
    else:
        return decimal_value


def xls_to_csv(file_path):
    new_file_path = file_path.replace('.xls', '.csv')
    wb = xlrd.open_workbook(file_path)
    sheet = wb.sheet_by_index(0)
    rows = []

    for row in range(sheet.nrows):
        csv_row = ''
        first_value = (sheet.cell(row, 1)).value
        for col in range(1, 8):
            value = str(sheet.cell(row, col).value)
            if type(first_value) == float:
                if col == 1:
                    station_id = 28079000 + int(value.split('.')[0])
                    value = str(station_id)
                if col == 4 or col == 5 and not (value is ''):
                    value = dms_to_decimal(value)
                elif col == 7 and not (value is ''):
                    value = get_type(value)
                csv_row += ';' + str(value)
        if not csv_row == '':
            csv_row = csv_row[1:] + '\r'
            rows.append(csv_row)
    result_rows = km.add_lookup_key(rows, '/home/angelfombellida/TFG/pruebas/files/key-estaciones-aire.txt')
    with open(new_file_path, 'w') as file:
        file.writelines(result_rows)
    os.remove(file_path)
    return new_file_path

def save_correlation(file_path):
    correlation = {}
    with open(file_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            correlation[line.split(';')[1]] = line.split(';')[0]
    with open('/data sources/lookup_correlation/estaciones-aire.pkl','wb') as f:
        pickle.dump(correlation,f)


get_file()
csv_file = xls_to_csv('/data sources/lookups/estaciones_aire.xls')
save_correlation(csv_file)
lookup = Lookup('estaciones_aire', historic_tag=None, con=con)
lookup.send_csv(csv_file, has_header=False, delimiter=';',
                headers=['EstacionAireKey', 'numero', 'estacion', 'direccion', 'longitud', 'latitud', 'altitud',
                         'tipo'], key='EstacionAireKey')
