import json
import re
import urllib3
from devo.sender import SenderConfigSSL, Sender, Lookup
from common import key_manager as km

engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)


def dms_to_decimal(coord):
    deg = coord[:2]
    min = coord[2:4]
    sec = coord[4:6]
    orientation = coord[-1]
    decimal_coord = float(deg) + (float(min) / 60) + (float(sec) / 3600)
    if orientation is 'O' or orientation is 'W' or orientation is 'S':
        return str(-decimal_coord)
    else:
        return str(decimal_coord)


def get_stations_data(city):
    mngr = urllib3.PoolManager()
    response = mngr.request('GET',
                            'https://opendata.aemet.es/opendata/api/valores/climatologicos/inventarioestaciones/todasestaciones/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZm9tYmVsbGlkYS45N0BnbWFpbC5jb20iLCJqdGkiOiJjMGQxNTUxNy01NGIzLTQwNDgtODY1NC1jODZjMGY1MzU2MzgiLCJpc3MiOiJBRU1FVCIsImlhdCI6MTU1MDA1Nzg5MSwidXNlcklkIjoiYzBkMTU1MTctNTRiMy00MDQ4LTg2NTQtYzg2YzBmNTM1NjM4Iiwicm9sZSI6IiJ9.Rj1Wk5VV4MZXvUMHE4WZ-Tm270gpyuT--jKRXDA9gdY')
    data = json.loads(response.data.decode('latin-1'))
    response = mngr.request('GET', data['datos'])

    data = json.loads(response.data.decode('latin-1'))
    rows = []
    regex = city.upper() + '((,| |/).+|$)'
    for item in data:
        if re.match(regex, item['nombre']):
            row = item['indicativo'] + ';' + item['nombre']  + ';' + dms_to_decimal(
                item['latitud']) + ';' + \
                  dms_to_decimal(item['longitud']) + ';' + item['altitud'] + '\r'
            rows.append(row)
    result_rows = km.add_lookup_key(rows,'/home/angelfombellida/TFG/pruebas/files/key-estaciones-tiempo.txt')
    return result_rows


def load_csv(stations):
    with open('/data sources/lookups/estaciones_tiempo.csv', 'w') as f:
        f.writelines(stations)


load_csv(get_stations_data('madrid'))
lookup = Lookup('estaciones_clima', historic_tag=None, con=con)
lookup.send_csv('/data sources/lookups/estaciones_tiempo.csv', has_header=False, delimiter=';',
                headers=['EstacionClimaKey','codEstacion', 'nombre','latitud','longitud','altitud'],
                key='EstacionClimaKey')
