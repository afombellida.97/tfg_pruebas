import json
import urllib3
from devo.sender import SenderConfigSSL, Sender, Lookup
import urllib.request as req
from common import key_manager as km
import pickle

engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)


def get_file():
    r = urllib3.PoolManager().request('GET', 'https://datos.madrid.es/egob/catalogo/keyword/estaciones.json')
    data = json.loads(r.data.decode('utf-8'))

    for item in data['result']['items']:
        if item['_about'] == 'https://datos.madrid.es/egob/catalogo/202468-0-intensidad-trafico':
            dist = item['distribution']
    for d in dist:
        if 'csv' in d['format']['value']:
            file_url = d['accessURL']
    req.urlretrieve(file_url, '/data sources/lookups/estaciones_trafico.csv')


def get_type(station_type):
    if station_type == '"URB"':
        return 'Urbano'
    else:
        return 'Interurbano M-30'


def transform_csv(file):
    with open(file, 'rb') as f:
        content = str(f.read(), 'iso-8859-1')
        lines = content.split('\n')
        lines = lines[1:-1]
        rows = []
        for l in lines:
            fields = l.split(';')
            row = get_type(fields[0]) + ';' + fields[2] + ';' + fields[4] + ';' + fields[7] + ';' + fields[8]
            rows.append(row)
        key_rows = km.add_lookup_key(rows, '/home/angelfombellida/TFG/pruebas/files/key-estaciones-trafico.txt')
    with open(file, 'w') as f:
        f.writelines(key_rows)

def save_correlation(file):
    correlation={}
    with open(file,'r') as f:
        lines = f.readlines()
        for line in lines:
            correlation[line.split(';')[2]] = line.split(';')[0]
    with open('/data sources/lookup_correlation/estaciones-trafico.pkl','wb') as f:
        pickle.dump(correlation,f)

get_file()
transform_csv('/data sources/lookups/estaciones_trafico.csv')
save_correlation('/data sources/lookups/estaciones_trafico.csv')
lookup = Lookup('estaciones_trafico', historic_tag=None, con=con)
lookup.send_csv('/data sources/lookups/estaciones_trafico.csv', has_header=False,
                delimiter=';', headers=['EstacionTraficoKey','tipo', 'idelem', 'nombre', 'longitud', 'latitud'], key='EstacionTraficoKey')
