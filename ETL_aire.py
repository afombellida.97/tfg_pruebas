import datetime

from devo.sender import SenderConfigSSL, Sender
import urllib.request as req
import urllib3
import json
import pickle
from common import dateutils as dtu, key_manager as km
import os
from devo.api import Client, JSON


def convert(value, m):
    convertibles = ['06', '42', '43', '44']
    if m in convertibles:
        return round(value * 1000, 5)
    else:
        return round(value, 5)


def get_rows(file):
    rows = []
    with open(file) as f:
        for line in f:
            fields = line.split(',')
            cod_estacion = fields[0] + fields[1] + fields[2]
            magnitud = fields[3]
            hora = 0
            for i in range(9, 57, 2):
                if fields[i + 1] == 'V':
                    date_time = fields[6] + '-' + fields[7] + '-' + fields[8] + ' ' + str(hora) + ':00:00'
                    utc_date_time = dtu.get_utc_datetime(date_time, offset=False)
                    msg = str(cod_estacion) + ';' + magnitud + ';' + str(convert(float(fields[i]), magnitud))
                    rows.append(str(utc_date_time) + '|' + msg)
                    hora += 1
    return rows


def get_file():
    mngr = urllib3.PoolManager()
    response = mngr.request('GET', 'https://datos.madrid.es/egob/catalogo/keyword/aire.json')

    data = json.loads(response.data.decode('utf-8'))

    for k in data['result']['items']:
        if k['_about'] == 'https://datos.madrid.es/egob/catalogo/212531-0-calidad-aire-tiempo-real':
            dist = k['distribution']

    for i in dist:
        if 'text' in i['format']['value']:
            file_url = i['accessURL']

    try:
        req.urlretrieve(file_url, 'data sources/datos.txt')
    except:
        print('Unable to get file')


def new_data(rows):
    row_keys = [row.split(';')[0] for row in rows]
    new_data = []
    last_sent = []
    try:
        with open('/home/angelfombellida/TFG/pruebas/files/state-aire.pkl', 'rb') as f:
            last_sent = pickle.load(f)
    except FileNotFoundError:
        print('File: state-aire.pkl will be created in /home/angelfombellida/TFG/pruebas/files')
    finally:
        if last_sent == []:
            new_data = rows
        else:
            if len(rows) < len(last_sent):
                new_data = rows
            else:
                for i in range(len(rows) - 1, -1, -1):
                    if not row_keys[i] == last_sent[-1]:
                        new_data.append(rows[i])
                    else:
                        break
                new_data = new_data[::-1]
        with open('/home/angelfombellida/TFG/pruebas/files/state-aire.pkl', 'wb') as f:
            pickle.dump(row_keys, f)

    return new_data


def set_correlation(new_data):
    with open('data sources/lookup_correlation/estaciones-aire.pkl', 'rb') as f:
        stations_correlation = pickle.load(f)

    with open('data sources/lookup_correlation/magnitudes-aire.pkl', 'rb') as f:
        magnitude_correlation = pickle.load(f)

    result = []
    for row in new_data:
        fields = row.split(';')
        if fields[1] in stations_correlation:
            fields[1] = stations_correlation[fields[1]]
        if fields[2] in magnitude_correlation:
            fields[2] = magnitude_correlation[fields[2]]
        result.append(';'.join(fields))
    return result


engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)

get_file()
rows = sorted(get_rows('data sources/datos.txt'))
sent = 0
new_data = new_data(rows)
new_data = km.add_key(new_data, '/home/angelfombellida/TFG/pruebas/files/key-aire.txt')

new_data = set_correlation(new_data)

for row in new_data:
    eventdate = row.split('|')[0]
    msg = row.split('|')[1]
    con.send('(usd)my.app.aytomadrid.aire', msg, date=eventdate)
    sent += 1

if sent > 0:
    print("\n[" + str(datetime.datetime.now()) + "] - Aire: EXECUTION FINISHED: " + str(sent) + " lines were sent")
else:
    print("\n[" + str(datetime.datetime.now()) + "] - Aire: nothing new")
