import datetime

import urllib3
import urllib.request as req
import xmltodict
import json
from devo.sender import SenderConfigSSL, Sender
import pickle
from common import dateutils as dtu, key_manager as km


def get_file():
    r = urllib3.PoolManager().request('GET', 'https://datos.madrid.es/egob/catalogo/keyword/incidencias.json')
    data = json.loads(r.data.decode('utf-8'))

    for item in data['result']['items']:
        if item['_about'] == 'https://datos.madrid.es/egob/catalogo/202062-0-trafico-incidencias-viapublica':
            dist = item['distribution']

    for i in dist:
        if 'xml' in i['format']['value']:
            file_url = i['accessURL']
    try:
        req.urlretrieve(file_url, 'data sources/incidencias.xml')
    except:
        print('Unable to get file')


def get_tipo(tipo):
    tipos = {'1': 'obras', '2': 'accidente', '3': 'alerta', '4': 'evento', '5': 'prevision', '10': 'contaminacion'}
    return tipos[tipo]


def get_rows(file):
    rows = []
    with open(file) as f:
        data = xmltodict.parse(f.read())
        for incidencia in data['Incidencias']['Incidencia']:
            if incidencia['incid_estado'] == '1':
                cod = incidencia['codigo']
                nombre = incidencia['nom_tipo_incidencia']
                f_inicio = str(
                    dtu.get_utc_datetime(incidencia['fh_inicio'].split('.')[0], middle_separator='T', offset=False))
                f_fin = str(
                    dtu.get_utc_datetime(incidencia['fh_final'].split('.')[0], middle_separator='T', offset=False))+'.000'
                prevista = incidencia['incid_prevista']
                planificada = incidencia['incid_planificada']
                lon = incidencia['longitud']
                lat = incidencia['latitud']
                tipo = get_tipo(incidencia['tipoincid'])
                msg = f_inicio + '|' + f_fin.split('+')[
                    0]  + ';' + cod + ';' + nombre + ';' + tipo + ';' + prevista + ';' + planificada + ';' + lat + ';' + lon
                rows.append(msg)
    return rows


def new_data(rows):
    row_keys = [row.split(';')[1] for row in rows]
    last_sent = []
    new_data = []
    try:
        with open('/home/angelfombellida/TFG/pruebas/files/state-incidencias.pkl', 'rb') as f:
            last_sent = pickle.load(f)
    except FileNotFoundError:
        print('File: state-aire.pkl will be created in /home/angelfombellida/TFG/pruebas/files')
    finally:
        if last_sent == []:
            new_data = rows
        else:
            for i, r in enumerate(row_keys):
                found = False
                for l in last_sent:
                    if r == l:
                        found = True
                if not found:
                    new_data.append(rows[i])
        with open('/home/angelfombellida/TFG/pruebas/files/state-incidencias.pkl', 'wb') as f:
            pickle.dump(row_keys, f)

            return new_data


engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)

get_file()

rows = get_rows('data sources/incidencias.xml')
new_data = new_data(rows)
new_data = km.add_key(new_data,'/home/angelfombellida/TFG/pruebas/files/key-incidencias.txt')
sent = 0
for row in new_data:
    eventdate = row.split('|')[0]
    msg = row.split('|')[1]
    con.send('(usd)my.app.aytomadrid.incidencias', msg, date=eventdate)
    sent += 1
if sent > 0:
    print("\n[" + str(datetime.datetime.now()) + "] - Incidencias: EXECUTION FINISHED: " + str(sent) + " lines were sent")
else:
    print("\n["+str(datetime.datetime.now())+"] - Incidencias: nothing new")