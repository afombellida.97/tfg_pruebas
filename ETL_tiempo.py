import datetime
from devo.api import Client, JSON
import urllib3
import json
import urllib.request as req
from devo.sender import SenderConfigSSL, Sender
import pickle
from common import dateutils as dtu, key_manager as km

mngr = urllib3.PoolManager()
station_correlation={}

def get_stations_id():
    stations = []
    global station_correlation
    api = Client(api_key='7d598c8a42bed8a502188ead562048e3a7f4fc6c57dd30da0594fb81460efaed',
                 api_secret='6d52969757df5c083a0484aa7c5b3cb3d61067c689568cc0c880f2f0a85216c4',
                 url='https://api-eu.logtrust.com/search/query',
                 user='angel.fombellida@devo.com',
                 app_name='calidad del aire Madrid')

    response = api.query(query="from my.lookuplist.estaciones_clima",
                         dates={'from': 'now() - 24*hour()'},
                         response='json',
                         processor=JSON)
    for item in response['object']:
       stations.append(item['codEstacion'])
       station_correlation[item['codEstacion']] = item['EstacionClimaKey']
    return stations


def get_files(stations):
    files = []
    for station in stations:
        response = mngr.request('GET',
                                'https://opendata.aemet.es/opendata/api/observacion/convencional/datos/estacion/' + station + '/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZm9tYmVsbGlkYS45N0BnbWFpbC5jb20iLCJqdGkiOiJjMGQxNTUxNy01NGIzLTQwNDgtODY1NC1jODZjMGY1MzU2MzgiLCJpc3MiOiJBRU1FVCIsImlhdCI6MTU1MDA1Nzg5MSwidXNlcklkIjoiYzBkMTU1MTctNTRiMy00MDQ4LTg2NTQtYzg2YzBmNTM1NjM4Iiwicm9sZSI6IiJ9.Rj1Wk5VV4MZXvUMHE4WZ-Tm270gpyuT--jKRXDA9gdY')
        data = json.loads(response.data.decode('utf-8'))

        if (data['estado'] == 200):
            req.urlretrieve(data['datos'], 'data sources/tiempo-' + station + '.json')
            files.append('data sources/tiempo-' + station + '.json')
        else:
            print('Could not get data from station ' + station)
    return files


def get_temperature_values(item):
    if 'tamax' in item:
        if 'tamin' in item:
            av_temp = round((float(item['tamax']) + float(item['tamin'])) / 2, 4)
            values = str(av_temp) + ';' + str(item['tamin']) + ';' + str(item['tamax'])
        else:
            values = str(item['tamax']) + ';' + ';' + str(item['tamax'])
    else:
        if 'tamin' in item:
            values = str(item['tamin']) + ';' + str(item['tamin']) + ';'
        else:
            values = ';' + ';'
    return values


def get_rows(file):
    fields = ['prec', 'hr', 'vmax', 'vv', 'pres', 'pres_nmar', 'inso', 'vis']
    rows = []
    with open(file) as f:
        data = json.loads(f.read())
        for item in data:
            if 'fint' in item:
                date_time = dtu.get_utc_datetime(item['fint'], middle_separator='T', offset=False)
                msg = str(date_time) + '|' + station_correlation[item['idema']] + ';' + get_temperature_values(item)

                for field in fields:
                    msg += ';'
                    if field in item:
                        msg += str(item[field])
                rows.append(msg)
    return rows


def new_data(rows):
    row_keys = [row.split(';')[0] for row in rows]
    new_data = []
    last_sent = []
    try:
        with open('/home/angelfombellida/TFG/pruebas/files/state-tiempo.pkl', 'rb') as f:
            last_sent = pickle.load(f)
    except FileNotFoundError:
        print('File: state-tiempo.pkl will be created in /home/angelfombellida/TFG/pruebas/files')
    finally:
        if last_sent == []:
            new_data = rows
        else:
            for i in range(len(rows) - 1, -1, -1):
                if not row_keys[i] == last_sent[-1]:
                    new_data.append(rows[i])
                else:
                    break
            new_data = new_data[::-1]
        with open('/home/angelfombellida/TFG/pruebas/files/state-tiempo.pkl', 'wb') as f:
            pickle.dump(row_keys, f)
    return new_data



engine_config = SenderConfigSSL(address='eu.elb.relay.logtrust.net',
                                port=443,
                                key='/home/angelfombellida/credenciales/angelfombellida.key',
                                cert='/home/angelfombellida/credenciales/angelfombellida.crt',
                                chain='/home/angelfombellida/credenciales/chain.crt')
con = Sender(engine_config)

rows = []
files = get_files(get_stations_id())
for f in files:
    rows += get_rows(f)

new_data = new_data(sorted(rows))
new_data = km.add_key(new_data,'/home/angelfombellida/TFG/pruebas/files/key-tiempo.txt')
sent = 0
for row in new_data:
    eventdate = row.split('|')[0]
    msg = row.split('|')[1]
    con.send('(usd)my.app.aytomadrid.tiempo', msg, date=eventdate)
    sent += 1

if sent > 0:
    print("\n[" + str(datetime.datetime.now()) + "] - Tiempo: EXECUTION FINISHED: " + str(sent) + " lines were sent")
else:
    print("\n[" + str(datetime.datetime.now()) + "] - Tiempo: nothing new")
